package polymorphismassignment;

public class SmartTelephone extends Telephone {

	public static void main(String[] args) {

		SmartTelephone st = new SmartTelephone();
		st.disconnected();
		st.with();
		st.lift();
		System.out.println("----------------------------------");
		Telephone st1 = new SmartTelephone();//Method over riding
		st1.with();
		st1.lift();
		st1.disconnected();

	}

	public void with() {
		super.with();//calling the parent class with method
		System.out.println("with Method from child class");
	}

	@Override
	public void lift() {
		System.out.println("This is Lift Method");
	}

	@Override
	public void disconnected() {
		System.out.println("Disconnect Method");

	}

}

package polymorphismassignment;

public abstract class Telephone {

	public void with() {
		System.out.println("With Method from super class");

	}

	public abstract void lift();

	public abstract void disconnected();

}

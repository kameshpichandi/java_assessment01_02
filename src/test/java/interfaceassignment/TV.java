package interfaceassignment;

public class TV implements TVremote {

	public static void main(String[] args) {
		TV tv = new TV();
		tv.onePlusTv();
		tv.samsungSmartTvRemort();
		tv.xiaomiSmartTvRemort();
		tv.lgTvRemote();
		tv.sonyTvRemote();

	}

	public void onePlusTv() {

		System.out.println("One Plus TV");
	}

	public void samsungSmartTvRemort() {
		System.out.println("Samsung smart TV Remote");
	}

	public void xiaomiSmartTvRemort() {
		System.out.println("Xiaomi Smart TV Remote");

	}

	public void lgTvRemote() {
		System.out.println("LG TV Remote");
	}

	public void sonyTvRemote() {
		System.out.println("Sony TV Remote");
	}

}

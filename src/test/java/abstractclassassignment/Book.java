package abstractclassassignment;

public abstract class Book {

	public abstract void write();
	
	public abstract void read();
	
}

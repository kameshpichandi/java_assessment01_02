package abstractclassassignment;

public class NoteBook extends Book {

	public void draw() {

		System.out.println("Draw the Diagram in the note Book");
	}

	public void write() {
		System.out.println("Write the importan Question in the Book");
	}

	public void read() {
		System.out.println("Read the importan question in the Book");

	}

	public static void main(String[] args) {

		NoteBook nb = new NoteBook();
		nb.write();
		nb.read();
		nb.draw();

	}

}

package stringassignment;

// Write a program to check if the word 'orange' is present in the "This is orange juice

public class CheckingTheWord {

	public static void main(String[] args) {

		String word = "This is orange juice";

		if (word.contains("orange")) {

			System.out.println("The Word Contains orange");
		} else {

			System.out.println("The Word not contains orange");
		}

	}

}

package stringassignment;

//Write a program to find the first and the last occurrence of the letter 'o' and character ',' in "Hello, World
public class OccurrenceOfLetter {

	public static void main(String[] args) {

		String word = "Hello World";

		char c = 'o';

		int index1 = word.indexOf(c);
		System.out.println("First occurrence of 'O' is at the index of :" + index1);

		int index2 = word.lastIndexOf(c);
		System.out.println("Last occurrence of 'O' is at the index of :" + index2);

	}

}

package listassignment;

import java.util.ArrayList;
import java.util.Iterator;
//1. Write a program to add 5 products name in a list and print the size of the list, 
//also display the contents of the list. Remove 2nd item from the list, 
//and after removal check if "Coke" is present in the list a. Use ArrayList

public class ArrayListDemo {

	public static void main(String[] args) {

		ArrayList<String> arraylist = new ArrayList<String>();
		arraylist.add("Maaza");
		arraylist.add("Sprite");
		arraylist.add("Foorti");
		arraylist.add("Fanta");
		arraylist.add("Coke");

		System.out.println("Size of the list befor removing second item : " + arraylist.size());

		Iterator<String> itr = arraylist.iterator();
		while (itr.hasNext()) {

			String list = itr.next();
			System.out.println(list);
		}
		arraylist.remove(1);
		System.out.println("After removing the second item");

		System.out.println("Size of the list after  removing second item : " + arraylist.size());

		Iterator<String> it = arraylist.iterator();
		while (it.hasNext()) {

			String list1 = it.next();
			System.out.println(list1);
		}

		System.out.println("Is Coke is present in the list : " + arraylist.contains("Coke"));
	}

}

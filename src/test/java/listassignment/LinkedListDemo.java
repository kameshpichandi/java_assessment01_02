package listassignment;

import java.util.Iterator;
import java.util.LinkedList;

public class LinkedListDemo {

	public static void main(String[] args) {

		LinkedList<String> linkedlist = new LinkedList<String>();

		linkedlist.add("Maaza");
		linkedlist.add("Sprite");
		linkedlist.add("Foorti");
		linkedlist.add("Fanta");
		linkedlist.add("Coke");
		System.out.println("Size of the Linkedlist befor removing second item : " + linkedlist.size());
		Iterator<String> itr = linkedlist.iterator();
		while (itr.hasNext()) {

			String list = itr.next();
			System.out.println(list);
		}
		linkedlist.remove(1);
		System.out.println("After removing the second item");
		System.out.println("Size of the Linkedlist after removing second item : " + linkedlist.size());

		Iterator<String> itr1 = linkedlist.iterator();
		while (itr1.hasNext()) {

			String list = itr1.next();
			System.out.println(list);
		}
		
		System.out.println("Is Coke is present in the list : " + linkedlist.contains("Coke"));


	}

}

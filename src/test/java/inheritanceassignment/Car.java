package inheritanceassignment;

public class Car extends SantroCar implements BasicCar {

	public void driver() {

		System.out.println("Car is on Driving Mode");
	}

	public void stop() {

		System.out.println("Car is on Stop Mode");
	}

	public void gearChange() {
		System.out.println("Change the gear of the car");
	}

	public void music() {
		System.out.println("play the music on the car");

	}

	public static void main(String[] args) {
		Car car = new Car();
		car.driver();
		car.stop();
		car.gearChange();
		car.music();
		car.remoteStart();

	}

}

package calculatingarearofrectangle;

public class Area extends Shape {

	public static void main(String[] args) {

		Area area = new Area();

		System.out.println("Area of Rectangle is : " + area.rectangleArea(5, 7));
		System.out.println("Area of Square is : " + area.squareArea(7));
		System.out.println("Area of circle is :" + area.circleArea(2));

	}

	@Override
	public int rectangleArea(int l, int b) {
		return l * b;
	}

	@Override
	public int squareArea(int side) {
		return side * side;
	}

	@Override
	public double circleArea(int radius) {
		return Math.PI * radius * radius;
	}

}

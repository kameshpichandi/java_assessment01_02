package calculatingarearofrectangle;
//create an abstract class 'Shape' with three abstract methods namely 'RectangleArea' taking two parameters, 
//'SquareArea' and 'CircleArea' taking one parameter each. The parameters of 'RectangleArea' are its length
//and breadth, that of 'SquareArea' is its side and that of 'CircleArea' is its radius. Now create another 
//class 'Area' containing all the three methods 'RectangleArea', 'SquareArea' and 'CircleArea' 
//for printing the area of rectangle, 
//square and circle respectively. Create an object of class 'Area' and call all the three methods.
public abstract class Shape {
	
	public abstract int rectangleArea(int l,int b);
	public abstract int squareArea(int side);
	public abstract double circleArea(int radius);
	
	

}
